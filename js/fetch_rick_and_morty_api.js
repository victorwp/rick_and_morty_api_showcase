// This function will fetch the Rick and Morty API and display the character Pickle Rick
async function findPickleRick() {
	let page = 1;
	let found = false;
	while (!found) {
	  const response = await fetch(`https://rickandmortyapi.com/api/character?page=${page}`);
	  const data = await response.json();
	  const characters = data.results;
	  for (const character of characters) {
		if (character.name === "Pickle Rick") {
		  console.log(character);
		  found = true;
		  return character;
		}
	  }
	  page++;
	  if (data.info.next === null) {
		console.log("Pickle Rick not found!");
		break;
	  }
	}
  }
  
// This function will display the character Pickle Rick
async function displayPickleRick() {
	const pickleRick = await findPickleRick();
	if (pickleRick) {
	  const div = document.createElement("div");
	  const img = document.createElement("img");
	  div.className = "character";
	  img.src = pickleRick.image;
	  img.alt = "Pickle Rick";
	  div.appendChild(img);
	  const ul = document.createElement("ul");

	  for (const key in pickleRick) {
		if (key !== "image" && key !== "origin" && key !== "location" && key !== "episode" && key !== "url") {
		  const li = document.createElement("li");
		  li.textContent = `${key}: ${pickleRick[key]}`;
		  ul.appendChild(li);
		}
	  }
	  div.appendChild(ul);
	  const body = document.querySelector("body");
	  body.appendChild(div);
	}
}