# Rick and Morty api showcase

## About the project

My Rick and Morty api showcase is a simple web app that uses the Rick and Morty api to show the characters and their details.

### Why?

This project is part of my web development portfolio, and I'd love to have your feedback on the project, code, structure or anything you can point out that could make me a better developer!



## Installation

To run the app locally, follow these steps:

## 1. Clone the repository:

```shell
git clone https://gitlab.com/victorwp/rick_and_morty_api_showcase
cd rick_and_morty_api_showcase
```



## 2. open the file

Open the index.html in your browser
.

## License

This project is licensed under the MIT License. See the [LICENSE](./LICENSE) file for more details.